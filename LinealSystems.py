import numpy as np
import math
from scipy.linalg import lu_factor, lu_solve


class Matrix:

    def setMatrix(self, list):
        """
        :param list:
        Матрица для заполнения
        Метод задает исходную матрицу, делает LU разложение, расчитывает ее ранк и определитель
        """
        self.list = list
        n = len(list)
        m = len(list[0])
        self.LU = Matrix(int(n), int(m), 0)
        self.LUP()
        self.rank = self.rank()
        self.det = self.det()

    # Не знаю, какой смысл был в таком определении, можно было засунуть все в один конструктор. Исправить
    def __init__(self, n, m, k):
        """
        :param n: кол-во строк
        :param m: кол-во столбцов
        :param k: индикатор, который сигнализирует о необходимости заполнения нулевой матрицей
        """
        self.rows = n
        self.columns = m
        self.isLUPCalculated = 0
        if k == 0:
            self.list = [[0] * m for i in range(n)]

    def solveByQR(self, b):
        """
        :param b: вектор b в линейной системе Ax=b
        :return: решение линейной системы
        Метод решения линейных систем с помощью QR разложения
        """
        Q, R = self.QR()
        b = np.dot(np.transpose(Q), b)
        x = [0] * n
        for i in reversed(range(self.rows)):
            s = sum(R[i][j] * x[j] for j in range(i + 1, n))
            x[i] = (b[i] - s) / R[i][i]
        return x

    def QR(self):
        """
        :return: QR разложение исходной матрицы преобразованием Хаусхолдера
        https://ru.wikipedia.org/wiki/%D0%9F%D1%80%D0%B5%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5_%D0%A5%D0%B0%D1%83%D1%81%D1%85%D0%BE%D0%BB%D0%B4%D0%B5%D1%80%D0%B0
        Подробнее о разложении - https://ru.wikipedia.org/wiki/QR-%D1%80%D0%B0%D0%B7%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5
        """
        n = self.rows
        R = np.identity(n)
        Q = np.identity(n)
        thisA = [[0] * n for i in range(n)]
        for i in range(n):
            for j in range(n):
                thisA[i][j] = self.list[i][j]
        d = self.rows - n
        if (self.rows == self.columns):
            for i in range(self.rows - 1):
                U = np.identity(self.rows)
                Uk = np.identity(n)
                E = np.identity(n)
                w = [0.0] * n
                s = 0
                for j in range(d, self.rows):
                    s += thisA[j][d] * thisA[j][d]
                w[0] = thisA[d][d] - math.sqrt(s)
                j = 1
                for k in range(d + 1, self.rows):
                    w[j] = thisA[k][d]
                    j += 1
                norm = 0
                for i in range(n):
                    norm += w[i] * w[i]
                Uk = E - np.dot(2 / norm, multVect(w))
                for k in range(d, self.rows):
                    for j in range(d, self.rows):
                        U[k][j] = Uk[k - d][j - d]
                Q = mulMatrix(Q, U)
                thisA = mulMatrix(U, thisA)
                d += 1
                n -= 1
        return Q, thisA

    def solveByHeidel(self, b, eps=10 ** -6):
        """
        :param b: вектор b в линейной системе виды Ax = b
        :param eps: порог ошибки, для критерия останова
        :return: приближенное решение системы
        подробнее о методе - https://ru.wikipedia.org/wiki/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4_%D0%93%D0%B0%D1%83%D1%81%D1%81%D0%B0_%E2%80%94_%D0%97%D0%B5%D0%B9%D0%B4%D0%B5%D0%BB%D1%8F_%D1%80%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D1%8F_%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D1%8B_%D0%BB%D0%B8%D0%BD%D0%B5%D0%B9%D0%BD%D1%8B%D1%85_%D1%83%D1%80%D0%B0%D0%B2%D0%BD%D0%B5%D0%BD%D0%B8%D0%B9
        """
        n = self.rows
        x = [.0 for i in range(n)]
        converge = False
        k = 0
        while not converge:
            k += 1
            x_new = np.copy(x)
            for i in range(n):
                s1 = sum(self.list[i][j] * x_new[j] for j in range(i))
                s2 = sum(self.list[i][j] * x[j] for j in range(i + 1, n))
                x_new[i] = (b[i] - s1 - s2) / self.list[i][i]
            converge = math.sqrt(sum((x_new[i] - x[i]) ** 2 for i in range(n))) <= eps
            x = np.copy(x_new)
        return x, k

    def solveByJacobi(self, b, eps=10 ** -6):
        """
        :param b: вектор b в линейной системе виды Ax = b
        :param eps: порог ошибки, для критерия останова
        :return: приближенное решение системы
        Подробнее о методе - https://ru.wikipedia.org/wiki/%D0%9C%D0%B5%D1%82%D0%BE%D0%B4_%D0%AF%D0%BA%D0%BE%D0%B1%D0%B8
        """
        n = self.rows
        x = [.0 for i in range(n)]
        x_new = [.0 for i in range(n)]
        converge = False
        k = 0
        while not converge or k != 10000:
            k += 1
            for i in range(n):
                s1 = sum(self.list[i][j] * x[j] for j in range(i))
                s2 = sum(self.list[i][j] * x[j] for j in range(i + 1, n))
                x_new[i] = (b[i] - s1 - s2) / self.list[i][i]
            converge = math.sqrt(sum((x_new[i] - x[i]) ** 2 for i in range(n))) <= eps
            x = np.copy(x_new)
        return x, k

    def LUP(self):
        """
        разложение исходной матрицы на LU, где  - нижнетреугольная, а U - верхнетреугольная матрицы
        Разложение идет с выбором ведущего элемента по столбцам и строкам. Изменения хранятся в матрицах P и Q соответственно
        Подробнее о разложении - https://ru.wikipedia.org/wiki/LU-%D1%80%D0%B0%D0%B7%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5
        :return:
        """
        if (self.isLUPCalculated == 0):
            self.isLUPCalculated = 1
            n = self.rows
            m = self.columns
            self.P = [i for i in range(n)]
            self.Q = [i for i in range(m)]
            for i in range(n):
                for j in range(m):
                    self.LU.list[i][j] = self.list[i][j]

            for i in range(m):
                max = 0
                maxRow = -1
                maxColumn = -1
                for row in range(i, n):
                    for column in range(i, m):
                        if (abs(self.LU.list[row][column]) > max):
                            max = abs((self.LU.list[row][column]))
                            maxColumn = column
                            maxRow = row
                if max != 0:
                    self.P[i], self.P[maxRow] = self.P[maxRow], self.P[i]
                    self.Q[i], self.Q[maxColumn] = self.Q[maxColumn], self.Q[i]
                    self.LU.list[i], self.LU.list[maxRow] = self.LU.list[maxRow], self.LU.list[i]
                    for k in range(n):
                        self.LU.list[k][i], self.LU.list[k][maxColumn] = self.LU.list[k][maxColumn], self.LU.list[k][i]
                    for j in range(i + 1, n):
                        if self.LU.list[i][i] != 0:
                            self.LU.list[j][i] /= self.LU.list[i][i]
                            for k in range(i + 1, m):
                                self.LU.list[j][k] -= self.LU.list[j][i] * self.LU.list[i][k]

    def det(self):
        """
        :return: Определитель матрицы
        """
        det = 1
        if self.rows == self.columns:
            for i in range(self.rows):
                det *= self.LU.list[i][i]
        else:
            print("Cannot solve the determinate: not a square matrix")
            return -1
        return det

    def rank(self):
        """
        :return: Ранк матрицы
        """
        rank = 0
        k = min(self.rows, self.columns)
        for i in range(k):
            if abs(self.LU.list[i][i]) > 10 ** (-14):
                rank += 1
            else:
                return rank
        return rank

    def solve(self, b):
        """
        :param b: вектор b в линейной системе виды Ax = b
        :return: решение слау с помощью LU разложения
        """
        rank = self.rank
        n = self.rows
        y = [0] * n
        z = [0] * n
        x = [0] * n
        newb = [0] * len(b)
        for i in range(len(b)):
            newb[i] = b[self.P[i]]
        for i in range(n):
            s = sum(self.LU.list[i][j] * y[j] for j in range(i))
            y[i] = newb[i] - s
        for i in reversed(range(rank)):
            s = sum(self.LU.list[i][j] * z[j] for j in range(i + 1, rank))
            z[i] = (y[i] - s) / self.LU.list[i][i]
        for i in range(n):
            x[self.Q[i]] = z[i]
        return x

    def isPossibleToSolve(self, b):
        """
        Проверка существования решения у системы с помощью теоремы Кронекера-Капели
        :param b: вектор b в линейной системе виды Ax = b
        :return: возможно ли решение слау
        """
        T = np.column_stack((self.list, b))
        if (np.linalg.matrix_rank(T) == np.linalg.matrix_rank(self.list)):
            return True
        else:
            return False

    def inverse(self):
        """
        :return: обратную матрицу к исходной с помощью метода solve (lu разложение)
        """
        revA = [[0] * self.columns for i in range(self.rows)]
        if (abs(self.det) > 10 ** (-14) and (self.rows == self.columns)):
            b = [0 for i in range(self.rows)]
            b[0] = 1
            x = [0] * n
            for j in range(self.columns):
                x = self.solve(b)
                for i in range(self.rows):
                    revA[i][j] = x[i]
                if j != self.columns - 1:
                    b[j], b[j + 1] = b[j + 1], b[j]
                print(b)
        else:
            print("Cannot solve: det=0 or not square matrix")
            return revA, False
        return revA, True

    def numberOfObyslovlennost(self, revA):
        """
        :param revA: обратная матрица
        :return: число обусловленности
        """
        s1 = 0
        for i in range(self.rows):
            s1 += np.max(self.list[i])
        s2 = 0
        for i in range(len(revA[i])):
            s2 += np.max(revA[i])
        return s1 * s2


def multVect(w):
    """
    :param w: вектор для умножения на себя транпонированного 
    :return: результат умножения
    """
    result = np.identity(len(w))
    for i in range(len(w)):
        for j in range(len(w)):
            result[i][j] = w[i] * w[j]
    return result


def printMatrix(list):
    """
    :param list: матрица для вывода
    """
    for i in range(len(list)):
        for j in range(len(list[i])):
            print("%.4f" % list[i][j], " ")
        print()


def mulMatrix(other, this):
    """
    :param other: одна мтарица размера n*m
    :param this: другая матрица размера m*k
    :return: результат умножения размером n*k
    """
    if (len(other[0]) == len(this)):
        n = len(other)
        m = len(other[0])
        r = len(this[0])
        A = [[0] * r for i in range(n)]
        for i in range(n):
            for k in range(r):
                for j in range(m):
                    A[i][k] += other[i][j] * this[j][k]
        return A
    else:
        print("Cannot multiply")


def generateMatrix(a, n, m):
    """
    :param a: индикатор необходимой матрицы. 2 - матрица с диагональным приоритетом
    3 - положительно определенная матрица
    4 - рандомная матрица
    :param n: кол-во строк
    :param m: кол-во столбцов
    :return: матрица размера n*m
    """
    list = np.random.randint(-100, 100, (n, m))
    if a == 2:
        if n == m:
            for i in range(n):
                sum = 0
                for j in range(m):
                    sum += abs(list[i][j])
                list[i][i] = abs(list[i][i]) + sum
        return list
    elif a == 3:
        return np.dot(list, np.transpose(list))
    elif a == 4:
        return list


a = 0
while a != 9:
    print("Enter the size of matrix nxm\n")
    n = int(input())
    m = int(input())
    print("First of all give me concrete matrix or use random\n1)Enter matrix\n2)Random matrix with diagonal priority\n"
          "3)Random positive deinite matrix\n4)Random matrix\n9)Exit")
    a = int(input())
    A = Matrix(int(n), int(m), 1)
    if a == 1:
        list = []
        for i in range(n):
            list.append([int(j) for j in input().split()])
        A.setMatrix(list)
    elif a == 2 or a == 3 or a == 4:
        A.setMatrix(generateMatrix(a, n, m))
    print("A - \n")
    printMatrix(A.list)
    while (a != 9):
        print("What do you want to do\n1)Rank by LUP\n2)Determinate by LUP\n3)To solve the system\n"
              "4)Find the reverse matrix by LUP\n5)Number of obyslovlenost'\n9)Exit")
        a = int(input())
        if (a == 1):
            print(A.rank)
        elif (a == 2):
            print(A.det)
        elif (a == 3):
            # b=[]
            print("Give vector b or use random\n1)Myself\n2)Random\n")
            a = int(input())
            if a == 1:
                b = []
                # b = [1.0, 2.0, 7.0]
                for i in range(A.rows):
                    b.append(int(input()))
            else:
                ind = True
                while (ind == True):
                    b = generateMatrix(a, n, 1)
                    if A.isPossibleToSolve(b):
                        ind = False
            print("Vector b - ", b)
            while (a != 9):

                x = [0] * n
                print("Which decomposition would you like to solve by?\n1)LUP\n"
                      "2)QR(only det!=0 and square matrix)\n3)Heidel and Jacobi(det!=0 and (matrix with diagonal priority or positive deinite matrix)"
                      "\n9)Exit")
                a = int(input())
                if (A.isPossibleToSolve(b)):
                    if a == 1:
                        x = A.solve(b)
                    elif a == 2:
                        if A.det != 0:
                            x = A.solveByQR(b)

                    elif a == 3:
                        if A.det != 0:
                            x, k1 = A.solveByHeidel(b, 10 ** (-14))
                            print("done")
                            print(x)
                            print("Heidel - ")
                            check = np.dot(A.list, x)
                            for i in range(n):
                                print(check[i] - b[i])
                            x, k2 = A.solveByJacobi(b, 10 ** (-14))
                            print("Heidel ", k1, " Jacobi ", k2)
                            print(x)
                    print(x)
                    print("Checking the result...")
                    check = np.dot(A.list, x)
                    for i in range(n):
                        print(check[i] - b[i])
        elif (a == 4) or (a == 5):
            #revA = [[0] * A.columns for i in range(A.rows)]
            revA, check = A.inverse()
            if (check == True):  # роверка на детерминант и квадратность
                if a == 5:
                    print(A.numberOfObyslovlennost(revA))
                else:
                    printMatrix(revA)
                    printMatrix(mulMatrix(revA, A.list))
                    printMatrix(mulMatrix(A.list, revA))
