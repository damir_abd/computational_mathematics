import numpy as np
from matplotlib import pyplot as plt

class SODU:
    def solute(self):
        h = 1 / 2 ** 5
        n = 5
        ylist = [[0] * 4 for i in range(n)]
        ylist1 = [[0] * 4 for i in range(n)]
        abstol = [0] * n
        hreal = [0] * n
        abstol2 = [0] * n
        hreal2 = [0] * n
        for t in range(n):
            count = round(5 / h)
            # h = 5/count
            x0 = 0
            y0 = [1, 1, 2, 1]
            y1 = [1, 1, 2, 1]
            y01 = [1, 1, 2, 1]
            K1 = func(x0, y0)
            K2 = self.K2(x0, y0, h, K1)
            Q2 = self.Q2(x0, y0, h, K1)
            Q3 = self.Q3(x0, y0, h, K1, Q2)
            for i in range(len(y0)):
                y1[i] = y0[i] + h * (self.b[0] * K1[i] + self.b[1] * K2[i])
                y01[i] = y0[i] + h * (1 / 6 * K1[i] + 4 / 6 * Q2[i] + 1 / 6 * Q3[i])
            y0 = np.copy(y1)
            y00 = np.copy(y01)
            x0 += h
            for o in range(count - 1):
                K1 = func(x0, y0)
                K2 = self.K2(x0, y0, h, K1)
                Q1 = func(x0, y00)
                Q2 = self.Q2(x0, y00, h, Q1)
                Q3 = self.Q3(x0, y00, h, Q1, Q2)
                for i in range(len(y0)):
                    y1[i] = y0[i] + h * (self.b[0] * K1[i] + self.b[1] * K2[i])
                    y01[i] = y00[i] + h * (1 / 6 * Q1[i] + 4 / 6 * Q2[i] + 1 / 6 * Q3[i])
                x0 += h
                y0 = np.copy(y1)
                y00 = np.copy(y01)
            hreal[t] = h
            abstol[t] = getAbsTol(y1)
            abstol2[t] = getAbsTol(y01)
            h = h / 2
            ylist[t] = np.copy(y1)
            ylist1[t] = np.copy(y01)
        max = 0
        for i in range(4):
            max += (ylist[3][i] - ylist[4][i]) ** 2
        max = 4 * np.sqrt(max) / 3
        hopt = hreal[3] * np.sqrt(3 * 10 ** (-4) / (max))

        h = 5 / round(5 / hopt)
        print("hopt for 1st method -", h)
        plt.subplot(111)
        plt.title("отношение ошибки к длине шага")

        plt.xlabel('шаг')
        plt.ylabel('ошибка')
        plt.plot(np.log10(hreal), np.log10(abstol), label='2ух этапный', color='red')
        plt.plot(np.log10(hreal), np.log10(abstol2), label='4ех этапный', color='blue')
        plt.legend()
        plt.show()
        count = round(5 / h)
        print("count of iterations - ", count)
        x0 = 0
        y0 = [1, 1, 2, 1]
        y1 = [1, 1, 2, 1]
        ylist11 = [[0] * 4 for j in range(int(count))]
        for o in range(int(count)):
            K1 = func(x0, y0)
            K2 = self.K2(x0, y0, h, K1)
            for i in range(len(y0)):
                y1[i] = y0[i] + h * (self.b[0] * K1[i] + self.b[1] * K2[i])
            x0 += h
            y0 = np.copy(y1)
            ylist11[o] = np.log10(getAbsTol(y1))
        x = np.arange(0, 5, h)
        plt.subplot(111)
        plt.title("отношение ошибки к независимой переменной")

        plt.plot(x, ylist11, color='red', label='первый мето;')
        print("absolute mistake - ", getAbsTol(y1))

        max = 0
        for i in range(4):
            max += (ylist1[3][i] - ylist1[4][i]) ** 2
        max = 8 * np.sqrt(max) / 7

        hopt = hreal[3] * np.sqrt(3 * 10 ** (-5) / (max))

        h = 5 / round(5 / hopt)
        print("hopt for 2nd method - ", h)
        plt.xlabel('независимая переменная')
        plt.ylabel('ошибка')
        count = round(5 / h)
        print("count of iterations - ", count)
        x0 = 0
        y00 = [1, 1, 2, 1]
        y01 = [1, 1, 2, 1]
        ylist2 = [[0] * 4 for j in range(int(count))]
        for o in range(int(count)):
            Q1 = func(x0, y00)
            Q2 = self.Q2(x0, y00, h, Q1)
            Q3 = self.Q3(x0, y00, h, Q1, Q2)
            for i in range(len(y0)):
                y01[i] = y00[i] + h * (1 / 6 * Q1[i] + 4 / 6 * Q2[i] + 1 / 6 * Q3[i])
            x0 += h
            y00 = np.copy(y01)
            ylist2[o] = np.log10(getAbsTol(y01))
        x = np.arange(0, 5, h)
        plt.plot(x, ylist2, 'b--', label='второй метод')
        plt.legend()
        plt.show()
        print("absolute mistake - ", getAbsTol(y01))

    def solveWithAutomatic3(self):
        n = []
        counts = [0] * 5
        mistakes = []
        ylist = []
        xlist = []
        xlisforacceptsteps = []
        xlisfornotacceptsteps = []
        hlistaccept = []
        hlistnotaacept = []
        for k in range(4, 9):
            # abstol = [0] * n
            rtol = 10 ** (-k)
            atol = 10 ** (-12)
            x0 = 0
            y00 = [1, 1, 2, 1]
            y01 = [1, 1, 2, 1]
            y00new = [1, 1, 2, 1]
            y01new = [1, 1, 2, 1]
            tol = rtol * np.sqrt(7) + atol
            f = func(x0, y00)
            maxf = 0
            for i in range(4):
                maxf += (f[i]) ** 2
            maxf = np.sqrt(maxf)
            delta = 1 / 625 + maxf ** 4
            h = (tol / delta) ** (1 / 4)
            u = [0] * 4
            maxu = 0
            for i in range(4):
                u[i] = y00[i] + h * f[i]
                maxu += (u[i]) ** 2
            maxu = np.sqrt(maxu)
            delta = 1 / 625 + maxu ** 4
            if h > (tol / delta) ** (1 / 4):
                h = (tol / delta) ** (1 / 4)
            counts[k - 4] = 1
            while (x0 + h <= 5):
                Q1 = func(x0, y00)
                Q2 = self.Q2(x0, y00, h, Q1)
                Q3 = self.Q3(x0, y00, h, Q1, Q2)
                for i in range(4):
                    y01[i] = y00[i] + h * (1 / 6 * Q1[i] + 4 / 6 * Q2[i] + 1 / 6 * Q3[i])
                y00new = np.copy(y00)
                for t in range(2):
                    Q1 = func(x0, y00new)
                    Q2 = self.Q2(x0, y00new, h / 2, Q1)
                    Q3 = self.Q3(x0, y00new, h / 2, Q1, Q2)
                    for i in range(len(y00)):
                        y01new[i] = y00new[i] + h / 2 * (1 / 6 * Q1[i] + 4 / 6 * Q2[i] + 1 / 6 * Q3[i])
                    if (t == 0):
                        y00new = np.copy(y01new)
                    # y00 = np.copy(y01new)
                    x0 += h / 2
                counts[k - 4] += 8
                max1 = 0
                max2 = 0
                max3 = 0
                for i in range(4):
                    max1 += y00[i] ** 2
                    max2 += y01[i] ** 2
                    max3 += y01new[i] ** 2
                tol = rtol * np.sqrt(max(max1, max2, max3)) + atol
                rn = 0
                for i in range(4):
                    rn += (y01new[i] - y01[i]) ** 2
                rn = 8 * np.sqrt(rn) / 7
                if (rn > 8 * tol):
                    if k == 6:
                        hlistnotaacept.append(h)
                        xlisfornotacceptsteps.append(x0)
                    x0 = x0 - h
                    h = h / 2
                    # y00 = np.copy(y00new)
                elif tol < rn:
                    if k == 6:
                        n.append(getAbsTol(y01new) / rn)
                        xlist.append(x0)
                        xlisfornotacceptsteps.append(x0)
                        hlistnotaacept.append(h)
                    y00 = np.copy(y01new)
                    h = h / 2
                elif tol / 16 <= rn:
                    if k == 6:
                        n.append(getAbsTol(y01) / rn)
                        hlistaccept.append(h)
                        xlisforacceptsteps.append(x0)
                        xlist.append(x0)
                    y00 = np.copy(y01)
                else:
                    if k == 6:
                        n.append(getAbsTol(y01) / rn)
                        hlistnotaacept.append(h)
                        xlisfornotacceptsteps.append(x0)
                        xlist.append(x0)
                    y00 = np.copy(y01)
                    h = 2 * h
                if k == 6:
                    mistakes.append(getAbsTol(y00))
                    ylist.append(y00)
            if (k == 6):
                plt.subplot(111)
                y = []
                for j in range(len(ylist)):
                    y.append(ylist[j][0])
                plt.plot(xlist, y, 'ro', ms=1.0, label='1ое уравнение')
                y = []
                for j in range(len(ylist)):
                    y.append(ylist[j][1])
                plt.plot(xlist, y, 'bo', ms=1.0, label='2ое уравнение')
                y = []
                for j in range(len(ylist)):
                    y.append(ylist[j][2])
                plt.plot(xlist, y, 'go', ms=1.0, label='3ье уравнение')
                y = []
                for j in range(len(ylist)):
                    y.append(ylist[j][3])
                plt.title("Решение")
                plt.plot(xlist, y, 'yo', ms=1.0, label='4ое уравнение')
                plt.show()
                plt.subplot(111)
                plt.title("Длина шага к независимой переменной")
                plt.plot(xlisforacceptsteps, hlistaccept, 'go', ms=0.5, label='ринятый шаг')
                plt.plot(xlisfornotacceptsteps, hlistnotaacept, 'ro', ms=0.5, label='непринятый шаг')
                plt.legend()
                plt.show()
                plt.subplot(111)
                plt.title("Ошибка к независимой переменной")
                plt.plot(xlist, mistakes)
                plt.show()
                plt.subplot(111)
                plt.title("Надежность")
                plt.plot(xlist, n)
                plt.show()
            print(getAbsTol(y01))

        plt.subplot(111)
        plt.title("степень относительной погрешности к количеству обращений к правой части")
        plt.plot(np.arange(4, 9), np.log10(counts))
        plt.show()

    # все сделано

    def solveWithAutomatic(self):
        n = []
        counts = [0] * 5
        mistakes = []
        ylist = []
        xlist = []
        xlisforacceptsteps = []
        xlisfornotacceptsteps = []
        hlistaccept = []
        hlistnotaacept = []

        rtol = 10 ** (-3)
        for k in range(4, 9):
            atol = 10 ** (-12)
            rtol /= 10
            x0 = 0
            y0 = [1, 1, 2, 1]
            y1 = [1, 1, 2, 1]
            y0new = [1, 1, 2, 1]
            y1new = [1, 1, 2, 1]
            tol = np.sqrt(7) * rtol + atol
            f = func(x0, y0)
            maxf = 0
            for i in range(4):
                maxf += (f[i]) ** 2
            delta = 1 / 125 + np.sqrt(maxf) ** 3

            h = (tol / delta) ** (1 / 3)
            u = [0] * 4
            maxu = 0
            for i in range(4):
                u[i] = y0[i] + h * f[i]
                maxu += (u[i]) ** 2
            delta = 1 / 125 + np.sqrt(maxu) ** 3
            if h > (tol / delta) ** (1 / 3):
                h = (tol / delta) ** (1 / 3)

            counts[k - 4] = 1
            while (x0 + h <= 5):
                K1 = func(x0, y0)  # первая итерация, считаем с обычным шагом
                K2 = self.K2(x0, y0, h, K1)
                for i in range(len(y0)):
                    y1[i] = y0[i] + h * (self.b[0] * K1[i] + self.b[1] * K2[i])
                y0new = np.copy(y0)  # просто новая переменная для значений игрик с предыдущей итерации
                for t in range(2):
                    K1 = func(x0, y0new)
                    K2 = self.K2(x0, y0new, h / 2, K1)
                    for i in range(len(y0)):
                        y1new[i] = y0new[i] + h / 2 * (self.b[0] * K1[i] + self.b[1] * K2[i])
                    y0new = np.copy(y1new)
                    # y0 = np.copy(y1new)
                    x0 += h / 2
                max1 = 0
                max2 = 0
                max3 = 0
                counts[k - 4] += 4
                for i in range(4):
                    max1 += y0[i] ** 2
                    max2 += y1[i] ** 2
                    max3 += y1new[i] ** 2
                tol = rtol * np.sqrt(max(max1, max2, max3)) + atol  # нахождение допуска по той формуле с rtol atol
                rn = 0
                for i in range(4):
                    rn += (y1new[i] - y1[i]) ** 2
                rn = 4 * np.sqrt(rn) / 3
                if (rn > 4 * tol):
                    if k == 6:
                        hlistnotaacept.append(h)
                        xlisfornotacceptsteps.append(x0)
                    x0 = x0 - h
                    h = h / 2
                    # y0 = np.copy(y0new)
                elif tol < rn:
                    if k == 6:
                        xlist.append(x0)
                        n.append(getAbsTol(y1new) / rn)
                        xlisfornotacceptsteps.append(x0)
                        hlistnotaacept.append(h)
                    y0 = np.copy(y1new)
                    h = h / 2
                elif tol / 8 <= rn:
                    if k == 6:
                        hlistaccept.append(h)
                        n.append(getAbsTol(y1) / rn)
                        xlisforacceptsteps.append(x0)
                        xlist.append(x0)
                    y0 = np.copy(y1)
                else:
                    if k == 6:
                        hlistnotaacept.append(h)
                        n.append(getAbsTol(y1) / rn)
                        xlisfornotacceptsteps.append(x0)
                        xlist.append(x0)
                    y0 = np.copy(y1)
                    h = 2 * h

                if k == 6:
                    mistakes.append(getAbsTol(y0))
                    ylist.append(y0)
            if (k == 6):
                plt.subplot(111)
                plt.title('Решение')
                y = []
                for j in range(len(ylist)):
                    y.append(ylist[j][0])
                plt.plot(xlist, y, 'ro', ms=1.0, label='1ое уравнение')
                y = []
                for j in range(len(ylist)):
                    y.append(ylist[j][1])
                plt.plot(xlist, y, 'bo', ms=1.0, label='2ое уравнение')
                y = []
                for j in range(len(ylist)):
                    y.append(ylist[j][2])
                plt.plot(xlist, y, 'go', ms=1.0, label='3ое уравнение')
                y = []
                for j in range(len(ylist)):
                    y.append(ylist[j][3])
                plt.plot(xlist, y, 'yo', ms=1.0, label='4ое уравнение')
                plt.legend()
                plt.show()
                plt.subplot(111)
                plt.title('Длина шага к независимой переменной')
                plt.plot(xlisforacceptsteps, hlistaccept, 'go', ms=2.0, label='принятые шаги')
                plt.plot(xlisfornotacceptsteps, hlistnotaacept, 'ro', ms=2.0, label='непринятые шаги')
                plt.legend()
                plt.show()
                plt.subplot(111)
                plt.title('Ошибка к независимой переменной')
                plt.plot(xlist, mistakes)
                plt.show()
                plt.subplot(111)
                plt.title('Надежность')
                plt.plot(xlist, np.log10(n))
                plt.show()

            print(getAbsTol(y1))


        plt.subplot(111)
        plt.title('степень относительной погрешности к количеству обращений к правой части')
        plt.plot(np.arange(4, 9), np.log10(counts))
        plt.show()

    def __init__(self):
        self.c = 0.55
        self.b = [1 / 11, 10 / 11]
        self.a = 0.55

    def K2(self, x0, y0, h, K1):
        y = np.copy(y0)
        for i in range(4):
            y[i] += h * self.a * K1[i]
        return func(x0 + self.c * h, y)

    def Q2(self, x0, y0, h, K1):
        y = [0] * 4
        for i in range(4):
            y[i] = y0[i] + h * 1 / 2 * K1[i]
        return func(x0 + 1 / 2 * h, y)

    def Q3(self, x0, y0, h, K1, K2):
        y = [0] * 4
        for i in range(4):
            z = 2 * h * K2[i]
            e = h * K1[i]
            y[i] = y0[i] + z - e
        return func(x0 + h, y)



 
    def YMRK(self):
        h = 1
        size = 10
        yall = [[0]] * size
        abstol = [0] * size
        hreal = [0] * size
        y11 = [0] * 4
        y12 = [0] * 4
        print(self.realSolution(5))
        for i in range(size):
            count = round(5 / h)
            h = 5 / count
            x0 = 0
            y0 = [1, 1, 2, 1]
            K1 = func(x0, y0)
            K21 = self.K2(x0, y0, h, K1)
            K22 = self.K22(x0, y0, h, K1)
            K3 = self.K3(x0, y0, h, K1, K22)
            y01 = [0] * 4
            y02 = [0] * 4
            for j in range(4):
                y01[j] = y0[j] + h * self.b[0] * K1[j] + h * self.b[1] * K21[j]
                y02[j] = y0[j] + h * (1 / 6 * K1[j] + 4 / 6 * K22[j] + 1 / 6 * K3[j])
            x0 += h
            z = 1
            while (z < count):
                K11 = func(x0, y01)
                K21 = self.K2(x0, y01, h, K11)
                K12 = func(x0, y02)
                K22 = self.K22(x0, y02, h, K12)
                K3 = self.K3(x0, y02, h, K12, K22)
                for j in range(4):
                    y11[j] = y01[j] + h * self.b[0] * K11[j] + h * self.b[1] * K21[j]
                    y12[j] = y02[j] + h * (1 / 6 * K12[j] + 4 / 6 * K22[j] + 1 / 6 * K3[j])

                x0 += h
                y01 = np.copy(y11)
                y02 = np.copy(y12)
                z += 1
            yall[i] = y01
            abstol[i] = getAbsTol(y01)
            print("tolerance - ", abstol[i])
            hreal[i] = h
            h = h / 2
        max = 0
        for i in range(4):
            if abs(yall[2][i] - yall[3][i]) > max:
                max = abs(yall[2][i] - yall[3][i])
        hopt = 3 * hreal[1] * 10 ** (-5) / (4 * max)
        print(hopt)
        hopt = 5 / round(5 / hopt)
        plt.plot(hreal, abstol, color='red')
        for i in range(size):
            abstol[i] = abstol[i] * 3
        print(hreal)
        print(abstol)
        plt.plot(hreal, abstol, color='blue')
        plt.show()

        # hopt
        count = round(5 / hopt)
        h = hopt
        x0 = 0
        y0 = [1, 1, 2, 1]
        K1 = func(x0, y0)
        K21 = self.K2(x0, y0, h, K1)
        K22 = self.K22(x0, y0, h, K1)
        K3 = self.K3(x0, y0, h, K1, K22)
        y01 = [0] * 4
        y02 = [0] * 4
        for j in range(4):
            y01[j] = y0[j] + h * self.b[0] * K11[j] + h * self.b[1] * K21[j]
            y02[j] = y0[j] + h * (1 / 6 * K12[j] + 4 / 6 * K22[j] + 1 / 6 * K3[j])
        x0 += h
        z = 1
        while (z < count):
            K11 = func(x0, y01)
            K12 = func(x0, y02)
            K21 = self.K2(x0, y01, h, K11)
            K22 = self.K22(x0, y02, h, K12)
            K3 = self.K3(x0, y02, h, K12, K22)
            for j in range(4):
                y11[j] = y01[j] + h * self.b[0] * K1[j] + h * self.b[1] * K21[j]
                y12[j] = y02[j] + h * (1 / 6 * K1[j] + 4 / 6 * K22[j] + 1 / 6 * K3[j])
            x0 += h
            y01 = np.copy(y11)
            y02 = np.copy(y12)
            z += 1
        print(getAbsTol(y01), y01)
def func(x0, y0):
        f = [0] * 4
        f[0] = (2 * x0 * y0[3]) / (y0[1])
        f[1] = -2 * x0 * y0[3] * np.exp(-1 / 2 * (y0[2] - 2))
        f[2] = 4 * x0 * y0[3]
        f[3] = -2 * x0 * np.log(y0[0])
        return f
def realSolution(x):
        y = [0] * 4
        y[0] = np.exp(np.sin(x ** 2))
        y[1] = np.exp(-np.sin(x ** 2))
        y[2] = 2 * np.sin(x ** 2) + 2
        y[3] = np.cos(x ** 2)
        return y
def getAbsTol(y0):
        max = 0
        y = realSolution(5)
        for i in range(4):
            max += (y[i] - y0[i]) ** 2
        return np.sqrt(max)


sodu = SODU()
sodu.solute()
print("absolute mistake with automatic (1st method) - ")
sodu.solveWithAutomatic()
print("absolute mistake with automatic (2nd method) - ")
sodu.solveWithAutomatic3()
