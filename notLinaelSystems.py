import math
import time
import sys
import numpy as np
from scipy.linalg import lu_factor, lu_solve

Complexity_lupq = (2.0 / 3.0) * 10 ** 3
Complexity_solve_lupq = 10 ** 2
Complexity_calc_jacobi = 10 ** 2
Complexity_calc_F = 10


def fillJacobiMatrix(x):
    """
    :param x: вектор
    :return: матрциа Якоби
    """
    cg = [[0] * 10 for i in range(10)]
    cg[0][0] = -np.sin(x[0] * x[1]) * x[1]
    cg[0][1] = -np.sin(x[0] * x[1]) * x[0]
    cg[0][2] = 3. * np.exp(- (3 * x[2]))
    cg[0][3] = x[4] * x[4]
    cg[0][4] = 2 * x[3] * x[4]
    cg[0][5] = -1
    cg[0][6] = 0
    cg[0][7] = -2. * np.cosh((2 * x[7])) * x[8]
    cg[0][8] = -np.sinh((2 * x[7]))
    cg[0][9] = 2
    cg[1][0] = np.cos(x[0] * x[1]) * x[1]
    cg[1][1] = np.cos(x[0] * x[1]) * x[0]
    cg[1][2] = x[8] * x[6]
    cg[1][3] = 0
    cg[1][4] = 6 * x[4]
    cg[1][5] = -np.exp(-x[9] + x[5]) - x[7] - 0.1e1
    cg[1][6] = x[2] * x[8]
    cg[1][7] = -x[5]
    cg[1][8] = x[2] * x[6]
    cg[1][9] = np.exp(-x[9] + x[5])
    cg[2][0] = 1
    cg[2][1] = -1
    cg[2][2] = 1
    cg[2][3] = -1
    cg[2][4] = 1
    cg[2][5] = -1
    cg[2][6] = 1
    cg[2][7] = -1
    cg[2][8] = 1
    cg[2][9] = -1
    cg[3][0] = - x[4] * pow(x[2] + x[0], -2.)
    cg[3][1] = -2. * np.cos(x[1] * x[1]) * x[1]
    cg[3][2] = - x[4] * pow(x[2] + x[0], -2.)
    cg[3][3] = -2. * np.sin(-x[8] + x[3])
    cg[3][4] = 1. / (x[2] + x[0])
    cg[3][5] = 0
    cg[3][6] = -2. * np.cos(x[6] * x[9]) * np.sin(x[6] * x[9]) * x[9]
    cg[3][7] = -1
    cg[3][8] = 2. * np.sin(-x[8] + x[3])
    cg[3][9] = -2. * np.cos(x[6] * x[9]) * np.sin(x[6] * x[9]) * x[6]
    cg[4][0] = 2 * x[7]
    cg[4][1] = -2. * np.sin(x[1])
    cg[4][2] = 2 * x[7]
    cg[4][3] = pow(-x[8] + x[3], -2.)
    cg[4][4] = np.cos(x[4])
    cg[4][5] = x[6] * np.exp(-x[6] * (-x[9] + x[5]))
    cg[4][6] = -(x[9] - x[5]) * np.exp(-x[6] * (-x[9] + x[5]))
    cg[4][7] = (2 * x[2]) + 2. * x[0]
    cg[4][8] = -pow(-x[8] + x[3], -2.)
    cg[4][9] = -x[6] * np.exp(-x[6] * (-x[9] + x[5]))
    cg[5][0] = np.exp(x[0] - x[3] - x[8])
    cg[5][1] = -3. / 2. * np.sin(3. * x[9] * x[1]) * x[9]
    cg[5][2] = -x[5]
    cg[5][3] = -np.exp(x[0] - x[3] - x[8])
    cg[5][4] = 2 * x[4] / x[7]
    cg[5][5] = -x[2]
    cg[5][6] = 0
    cg[5][7] = -x[4] * x[4] * pow(x[7], (-2))
    cg[5][8] = -np.exp(x[0] - x[3] - x[8])
    cg[5][9] = -3. / 2. * np.sin(3. * x[9] * x[1]) * x[1]
    cg[6][0] = np.cos(x[3])
    cg[6][1] = 3. * x[1] * x[1] * x[6]
    cg[6][2] = 1
    cg[6][3] = -(x[0] - x[5]) * np.sin(x[3])
    cg[6][4] = np.cos(x[9] / x[4] + x[7]) * x[9] * pow(x[4], (-2))
    cg[6][5] = -np.cos(x[3])
    cg[6][6] = pow(x[1], 3.)
    cg[6][7] = -np.cos(x[9] / x[4] + x[7])
    cg[6][8] = 0
    cg[6][9] = -np.cos(x[9] / x[4] + x[7]) / x[4]
    cg[7][0] = 2. * x[4] * (x[0] - 2. * x[5])
    cg[7][1] = -x[6] * np.exp(x[1] * x[6] + x[9])
    cg[7][2] = -2. * np.cos(-x[8] + x[2])
    cg[7][3] = 0.15e1
    cg[7][4] = pow(x[0] - 2. * x[5], 2.)
    cg[7][5] = -4. * x[4] * (x[0] - 2. * x[5])
    cg[7][6] = -x[1] * np.exp(x[1] * x[6] + x[9])
    cg[7][7] = 0
    cg[7][8] = 2. * np.cos(-x[8] + x[2])
    cg[7][9] = -np.exp(x[1] * x[6] + x[9])
    cg[8][0] = -3
    cg[8][1] = -2. * x[7] * x[9] * x[6]
    cg[8][2] = 0
    cg[8][3] = np.exp((x[4] + x[3]))
    cg[8][4] = np.exp((x[4] + x[3]))
    cg[8][5] = -0.7e1 * pow(x[5], -2.)
    cg[8][6] = -2. * x[1] * x[7] * x[9]
    cg[8][7] = -2. * x[1] * x[9] * x[6]
    cg[8][8] = 3
    cg[8][9] = -2. * x[1] * x[7] * x[6]
    cg[9][0] = x[9]
    cg[9][1] = x[8]
    cg[9][2] = -x[7]
    cg[9][3] = np.cos(x[3] + x[4] + x[5]) * x[6]
    cg[9][4] = np.cos(x[3] + x[4] + x[5]) * x[6]
    cg[9][5] = np.cos(x[3] + x[4] + x[5]) * x[6]
    cg[9][6] = np.sin(x[3] + x[4] + x[5])
    cg[9][7] = -x[2]
    cg[9][8] = x[1]
    cg[9][9] = x[0]
    return cg


def fillF(x):
    F = [0] * 10
    F[0] = np.cos(x[0] * x[1]) - np.exp(-3. * x[2]) + x[3] * x[4] * x[4] - x[5] - \
           np.sinh(2. * x[7]) * x[8] + 2. * x[9] + 2.0004339741653854  # 440

    F[1] = np.sin(x[0] * x[1]) + x[2] * x[8] * x[6] - np.exp(-x[9] + x[5]) + \
           3. * x[4] * x[4] - x[5] * (x[7] + 1.) + 10.8862720364070199  # 94

    F[2] = x[0] - x[1] + x[2] - x[3] + x[4] - x[5] + x[6] - x[7] + x[8] - x[9] - 3.1361904761904761  # 904

    F[3] = 2. * np.cos(-x[8] + x[3]) + x[4] / (x[2] + x[0]) - np.sin(x[1] * \
                                                                     x[1]) + np.cos(x[6] * x[9]) * np.cos(x[6] * x[9]) - \
           x[7] - 0.1707472705022304  # 75

    F[4] = np.sin(x[4]) + 2. * x[7] * (x[2] + x[0]) - np.exp(-x[6] * \
                                                             (-x[9] + x[5])) + 2. * np.cos(x[1]) - 1. / (
                       x[3] - x[8]) - 0.3685896273101277  # 86

    F[5] = np.exp(x[0] - x[3] - x[8]) + x[4] * x[4] / x[7] + 0.5 * \
           np.cos(3. * x[9] * x[1]) - x[5] * x[2] + 2.0491086016771875  # 11

    F[6] = x[1] * x[1] * x[1] * x[6] - np.sin(x[9] / x[4] + x[7]) + \
           (x[0] - x[5]) * np.cos(x[3]) + x[2] - 0.7380430076202798  # 01

    F[7] = x[4] * (x[0] - 2. * x[5]) * (x[0] - 2. * x[5]) - 2. * np.sin(-x[8] + \
                                                                        x[2]) + 1.5 * x[3] - np.exp(
        x[1] * x[6] + x[9]) + 3.5668321989693809  # 04

    F[8] = 7. / x[5] + np.exp(x[4] + x[3]) - 2 * x[1] * x[7] * x[9] * x[6] + \
           3 * x[8] - 3 * x[0] - 8.4394734508383257  # 49

    F[9] = x[9] * x[0] + x[8] * x[1] - x[7] * x[2] + np.sin(x[3] + x[4] + \
                                                            x[5]) * x[6] - 0.7823809523809523  # 809
    return F


def fillF1(x):
    F = [0] * 2
    F[0] = np.sin(x[1] - 1) + x[0] - 1.3
    F[1] = x[1] - np.sin(x[0] + 1) - 0.8
    return F


def fillJacobiMatrix1(x):
    cg = [[0] * 2 for i in range(2)]
    cg[0][0] = 1
    cg[0][1] = np.cos(x[1] - 1)
    cg[1][0] = -np.cos(x[0] + 1)
    cg[1][1] = 1
    return cg


def solveByNewton(x_k, lim=1000, rep=1):
    """
    :param x_k: начальное приближение
    :param lim: максимальное кол-во итераций
    :param rep: что-то, что я не помню
    :return: Решение СНАУ, кол-во итераций, время, сложность (кол-во операций)
    """
    start_time = time.clock()
    error = 1
    x_knext = np.copy(x_k)
    k = 0
    Complexity = 0
    while (error > 10 ** (-5) and k < 100):
        if k <= lim:
            if k % rep == 0:
                try:
                    A = fillJacobiMatrix(x_k)
                except RuntimeWarning:
                    return [0] * 10, 0, 1000000, 0
                try:
                    lu, piv = lu_factor(A)
                except ValueError:
                    return [0] * 10, 0, 1000000, 0
                Complexity += Complexity_calc_jacobi
                Complexity += Complexity_lupq
        try:
            b = fillF(x_k)
        except RuntimeWarning:
            return [0] * 10, 0, 1000000, 0
        Complexity += Complexity_calc_F
        for i in range(10):
            b[i] = -b[i]
        try:
            dx = lu_solve((lu, piv), b)
        except ValueError:
            return [0] * 10, 0, 1000000, 0
        Complexity += Complexity_solve_lupq
        x_knext = x_k + dx
        error = 0
        for i in range(10):
            if (abs(x_k[i] - x_knext[i]) > error):
                error = abs(x_k[i] - x_knext[i])
        x_k = np.copy(x_knext)
        k += 1
    return x_k, k, time.clock() - start_time, Complexity


def solveByNewton1(x_k):
    """
    Решение без пересчета матрицы Якоби
    :param x_k: приближение
    :return: решение, время
    """
    start_time = time.clock()
    error = 1
    x_knext = np.copy(x_k)
    k = 0
    while (error > 10 ** (-5)):
        k += 1
        A = fillJacobiMatrix1(x_k)
        b = fillF1(x_k)
        for i in range(2):
            b[i] = -b[i]
        dx = np.linalg.solve(A, b)
        x_knext = x_k + dx
        error = 0
        for i in range(2):
            if (abs(x_k[i] - x_knext[i]) > error):
                error = abs(x_k[i] - x_knext[i])
        x_k = np.copy(x_knext)
    return x_k, k, time.clock() - start_time


def checkAnswer(x):
    try:
        F = fillF(x)
    except RuntimeWarning:
        return [0] * 10, 0, 1000000, 0

    for i in range(len(x)):
        if F[i] > 10 ** (-6):
            return False
    return True


# solution = [0.3730, 0.5938, 1.610297, -0.56065, -0.34394, 1.282066, 0.22015, -0.229961, 1.253557, -1.10835]

x_k = [1.0, 2.0]
print("First task - ")
x, k, proc_time = solveByNewton1(x_k)
print("That's the solution - ")
print(x)
print("That's the count of iterations - ", k)
print("That's the time: ", proc_time)
# print(timeit.timeit("solveByNewton1(x_k)", setup="from __main__ import solveByNewton1,x_k", number=1))
print("\n\n\n")
x_k = [0.5, 0.5, 1.5, -1.0, -0.5, 1.5, 0.5, -0.5, 1.5, -1.5]
x, k, proc_time, Complexity = solveByNewton(x_k)
print("That's the solution - ")
print(x)
print("That's the count of iterations - ", k)
print("That's the count of operations - ", Complexity)
print("That's the time: ", proc_time)
# print(timeit.timeit("solveByNewton(x_k)", setup="from __main__ import solveByNewton,x_k", number=1))
print("\n\n\n")
lim = 0
x, k, proc_time, Complexity = solveByNewton(x_k, lim)
print("That's the solution by modified method (solving Jacobi only once) - ")
print(x)
print("That's the count of iterations - ", k)
print("That's the count of operations - ", Complexity)
print("That's the time: ", proc_time)
# timeit.timeit("solveByNewton(x_k,lim)", setup="from __main__ import solveByNewton,x_k,lim", number=1))
print("\n\n\n")
rep = 2
x, k, proc_time, Complexity = solveByNewton(x_k, rep=rep)
print("That's the solution by modified method (solving Jacobi every two time) - ")
print(x)
print("That's the count of iterations - ", k)
print("That's the count of operations - ", Complexity)
print("That's the time: ", proc_time)
# timeit.timeit("solveByNewton(x_k,rep=rep)", setup="from __main__ import solveByNewton,x_k,rep", number=1))
print("\n\n\n")
min = 10.0
minlim = 0
minrep = 0
for lim in range(10):
    x, k, proc_time, Complexity = solveByNewton(x_k, lim)
    # z = timeit.timeit("solveByNewton(x_k,lim)", setup="from __main__ import solveByNewton,x_k,lim", number=1)
    if proc_time < min and k < 100 and checkAnswer(x):
        min = proc_time
        minlim = lim
        minrep = 1
    for rep in range(2, lim):
        x, k, proc_time, Complexity = solveByNewton(x_k, lim, rep)
        # z = timeit.timeit("solveByNewton(x_k,lim,rep)", setup="from __main__ import solveByNewton,x_k,lim,rep", number=1)
        if proc_time < min and k < 100 and checkAnswer(x):
            min = proc_time
            minlim = lim
            minrep = rep
print("Optimal limit is - ", minlim, "Optimal repeat is - ", minrep, "\n and time for it is - ", min)

x_k[4] = -0.2
print("After x5 = -0.2")
x, k, proc_time, Complexity = solveByNewton(x_k)
print("That's the solution - ")
print(x)
print("That's the count of iterations - ", k)
print("That's the count of operations - ", Complexity)
print("That's the time: ", proc_time)
# print(timeit.timeit("solveByNewton(x_k)", setup="from __main__ import solveByNewton,x_k", number=1))
print("\n\n\n")
lim = 0
x, k, proc_time, Complexity = solveByNewton(x_k, lim)
print("That's the solution by modified method (solving Jacobi only once) - ")
print(x)
print("That's the count of iterations - ", k)
print("That's the count of operations - ", Complexity)
print("That's the time: ", proc_time)
# timeit.timeit("solveByNewton(x_k,lim)", setup="from __main__ import solveByNewton,x_k,lim", number=1))
print("\n\n\n")
lim = 2
x, k, proc_time, Complexity = solveByNewton(x_k, lim)
print("That's the solution by modified method (solving Jacobi every two time) - ")
print(x)
print("That's the count of iterations - ", k)
print("That's the count of operations - ", Complexity)
print("That's the time: ", proc_time)

lim = 8
x, k, proc_time, Complexity = solveByNewton(x_k, lim)
print("That's the solution by modified method (solving Jacobi every eight time) - ")
print(x)
print("That's the count of iterations - ", k)
print("That's the count of operations - ", Complexity)
print("That's the time: ", proc_time)

lim = 10
x, k, proc_time, Complexity = solveByNewton(x_k, lim)
print("That's the solution by modified method (solving Jacobi every ten time) - ")
print(x)
print("That's the count of iterations - ", k)
print("That's the count of operations - ", Complexity)
print("That's the time: ", proc_time)
# timeit.timeit("solveByNewton(x_k,rep=rep)", setup="from __main__ import solveByNewton,x_k,rep", number=1))
print("\n\n\n")
min = 10.0
minlim = 0
minrep = 0
for lim in range(10):
    x, k, proc_time, Complexity = solveByNewton(x_k, lim)
    # z = timeit.timeit("solveByNewton(x_k,lim)", setup="from __main__ import solveByNewton,x_k,lim", number=1)
    if proc_time < min and k < 100 and checkAnswer(x):
        min = proc_time
        minlim = lim
        minrep = 1
    for rep in range(2, lim):
        x, k, proc_time, Complexity = solveByNewton(x_k, lim, rep)
        # z = timeit.timeit("solveByNewton(x_k,lim,rep)", setup="from __main__ import solveByNewton,x_k,lim,rep", number=1)
        if proc_time < min and k < 100 and checkAnswer(x):
            min = proc_time
            minlim = lim
            minrep = rep
print("Optimal limit is - ", minlim, "Optimal repeat is - ", minrep, "\n and time for it is - ", min)
print(x)
try:
    print(fillF(x))
except RuntimeWarning:
    print("Unable to solve")
